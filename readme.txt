In /images folder create subfolder /Raw and extract the archive with the training data
If you want to clasify images in folders go to DataSelectors class and set setImagesinFolders to True


### Pipeline Idea:
1) All processers extend the RoofProcesser abstract class
2) they get called via processer.process(roofs) and they add their features to the feature map for each individual roof.
Note that they can access features from the previous processers so we can do some cascading features.
3) We do some training based on the feature map
4) We apply the pipeline for the eval images
5) we classify based on the extracted features

---

In order to read the features from files, use:
processer.process_and_store() instead of processer.process()

If you want to save new features:
- uncomment only the class you want to save features for in the pipeline
- set WRITE_TO_FILE = True in the class RoofProcesser

When using the features:
- set back WRITE_TO_FILE = False