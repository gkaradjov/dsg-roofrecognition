__author__ = 'martin'
import numpy as np
from skimage.transform import (hough_line, hough_line_peaks, hough_circle)
from skimage.feature import canny,peak_local_max
from skimage.draw import circle_perimeter

from skimage import color
from PIL import ImageDraw
import matplotlib.pyplot as plt
import matplotlib
from src.RoofRecognition.RoofProcesser import RoofProcesser
import math
from collections import defaultdict

class HoughTransform(RoofProcesser):

    def __init__(self, cannyThreshold = 1.4, angleThreshold = 0.174):
        self.classFeats = defaultdict(lambda:[])
        self.cannyThreshold = cannyThreshold
        self.angleThreshold = angleThreshold

    def process(self, roofs):
        for (i,roof) in enumerate(roofs):
            if (i%500 ==0):
                print("Processed "+str(i)+"/"+str(len(roofs))+" roofs")
            self.transform(roof)

    def transform(self, roof):
        img = roof.getGrayScale()
        imgD = np.array(img)
        edges = canny(imgD, self.cannyThreshold)
        #print(matplotlib.get_backend())
        horizontals = 0
        verticals = 0
        diagonals = 0
        main_diagonals = 0
        aux_diagonals = 0
        lines = hough_line(edges)
        hspace,angles,dists = hough_line_peaks(lines[0],lines[1],lines[2])
        if len(angles) == 0:
            roof.features['horizontal'] = horizontals
            roof.features['vertical'] = verticals
            roof.features['diagonal'] = diagonals
            #   roof.features['meanAngle'] = 0
            roof.features['main_diagonals'] = 0
            roof.features['aux_diagonals'] = 0

        else:
            draw = ImageDraw.Draw(img)
            for i in range(min(5,len(angles))):
                rho = dists[i]
                theta = angles[i]
                if abs(theta) <= self.angleThreshold:
                    verticals +=1
                elif abs(theta) >= math.pi/2 - self.angleThreshold and abs(theta) <= math.pi/2 + self.angleThreshold:
                    horizontals +=1
                elif abs(theta) >= math.pi/4 - self.angleThreshold and abs(theta)  <= math.pi/4 +self.angleThreshold:
                    diagonals +=1
                    if (theta>0):
                        aux_diagonals +=1
                    else:
                        main_diagonals +=1
                    #print(theta)
                    #self.drawLine(draw, rho, theta)
                    #0.78 => auxiliary
                    # -0.88 => main
            #plt.imshow(img)
            #plt.show()
            #if diagonals>0:
            #    img.show()
            roof.features['horizontal'] = horizontals
            roof.features['vertical'] = verticals
            roof.features['diagonal'] = diagonals
            #roof.features['meanAngle'] = np.mean(angles)
            roof.features['main_diagonals'] = main_diagonals
            roof.features['aux_diagonals'] = aux_diagonals
        hough_radii = np.arange(15, 30, 2)
        hough_res = hough_circle(edges, hough_radii)
        centers = []
        accums = []
        radii = []

        for radius, h in zip(hough_radii, hough_res):
            # For each radius, extract two circles
            num_peaks = 2
            peaks = peak_local_max(h, num_peaks=num_peaks)
            centers.extend(peaks)
            accums.extend(h[peaks[:, 0], peaks[:, 1]])
            radii.extend([radius] * num_peaks)
        suma = 0
        for idx in np.argsort(accums)[::-1][:5]:
            suma += accums[idx]
            #center_x, center_y = centers[idx]
            #radius = radii[idx]
            #draw.ellipse((center_x-radius,center_y-radius,center_x+radius,center_y+radius))
        roof.features['houghCircleIntegral'] = suma
        #self.classFeats[roof.c].append(suma)


    def drawLine(self, draw, rho, theta):
        a = np.cos(theta)
        b = np.sin(theta)
        x0 = a * rho
        y0 = b * rho
        x1 = int(x0 + 1000 * (-b))
        y1 = int(y0 + 1000 * (a))
        x2 = int(x0 - 1000 * (-b))
        y2 = int(y0 - 1000 * (a))
        draw.line((x1, y1, x2, y2), fill=0, width=2)

