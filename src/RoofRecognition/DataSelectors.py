import csv, os, shutil
from PIL import Image

class DataSelectors:
    imageDictionary = dict()
    rootRawImagesPath = '../../images/Raw/'

    def __init__(self, train_dataset='id_train_boosted.csv'):
        print('Begin init...')
        #config
        setImagesinFolders = False
        self.train_dataset = train_dataset

        #load train dictionary
        with open('../../images/'+self.train_dataset) as csvfile:
            spamreader = csv.reader(csvfile, delimiter=',')
            for row in spamreader:
                self.imageDictionary[row[0]] = row[1]
        if setImagesinFolders:
            self.clasify_in_folders()   

    def get_image_ids_eval(self):
        with open('../../images/id_eval.csv') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=',')
            r = [x[0] for x in spamreader]
        return r


    def clasify_in_folders(self):
        print('Clasifying images in folders')
        rootDirectory = '../../images/ImagesInClasses'
        class1Name = '/Class1'
        class2Name = '/Class2'
        class3Name = '/Class3'
        class4Name = '/Class4'

        if not os.path.exists(rootDirectory):
            os.makedirs(rootDirectory)
        if not os.path.exists(rootDirectory + class1Name):
            os.makedirs(rootDirectory + class1Name)

        if not os.path.exists(rootDirectory + class2Name):
            os.makedirs(rootDirectory + class2Name)

        if not os.path.exists(rootDirectory + class3Name):
            os.makedirs(rootDirectory + class3Name)

        if not os.path.exists(rootDirectory + class4Name):
            os.makedirs(rootDirectory + class4Name)


        class1Images = self.get_image_ids_of_class(1)
        for c1I in class1Images:
            shutil.copy2(self.rootRawImagesPath + c1I + '.jpg', rootDirectory + class1Name + '/' + c1I + '.jpg')

        class2Images = self.get_image_ids_of_class(2)
        for c2I in class2Images:
            shutil.copy2(self.rootRawImagesPath + c2I + '.jpg', rootDirectory + class2Name + '/' + c2I + '.jpg')

        class3Images = self.get_image_ids_of_class(3)
        for c3I in class3Images:
            shutil.copy2(self.rootRawImagesPath + c3I + '.jpg', rootDirectory + class3Name + '/' + c3I + '.jpg')

        class4Images = self.get_image_ids_of_class(4)
        for c4I in class4Images:
            shutil.copy2(self.rootRawImagesPath + c4I + '.jpg', rootDirectory + class4Name + '/' + c4I + '.jpg')

    def getImage(self, name):
        im = Image.open(self.rootRawImagesPath + name + ".jpg")
        result = im.copy()
        im.close()
        return result

    def get_image_ids_of_class(self, image_class):
        dicti = self.imageDictionary.items()
        d = dict((k, v) for k, v in self.imageDictionary.items() if v == str(image_class))
        return d
      