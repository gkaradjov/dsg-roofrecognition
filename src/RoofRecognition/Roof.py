__author__ = 'martin'

class Roof:

    def __init__(self, id, img, c):
        self.id = id
        self.img = img
        self.c = c
        self.features = {}
        self.prediction = 4
        self.gray = None
        self.resizedGray = None

    def getGrayScale(self):
        if self.gray is None:
            self.gray = self.img.convert('L')
        return self.gray

    def getResizedGrayScale(self):
        if self.resizedGray is None:
            self.resizedGray = self.getGrayScale().resize((130,130))
        return self.resizedGray