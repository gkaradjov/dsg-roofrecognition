import scipy
import scipy.misc
import scipy.cluster
import webcolors
import numpy
from PIL import Image, ImageDraw

NUMBER_OF_FREQUENT_COLORS = 20
NUM_CLUSTERS = 1
from src.RoofRecognition.RoofProcesser import RoofProcesser


class RoofProcesserFrequentColors(RoofProcesser):

    def process(self, roofs):
        for (i, roof) in enumerate(roofs):
            colors = roof.img.getcolors(roof.img.size[0]*roof.img.size[1])
            freq_colors = []
            for i in range(0, NUMBER_OF_FREQUENT_COLORS):
                max_color = max(colors)
                colors.remove(max_color)
                # max_color = (183, (23, 15, 56))
                freq_colors.append(max_color[1][0])
                freq_colors.append(max_color[1][1])
                freq_colors.append(max_color[1][2])
            roof.features['freq_colors'] = numpy.array(freq_colors)

class RoofProcesserColor(RoofProcesser):
    def __init__(self):
        self.color_ids = {}
        self.ids = 0

    # TODO precompute colors for each image,, otherwise very slow
    def process(self, roofs):
        print('Calculating of color feature...')
        #TODO define a dictionary of colors, some rare colors may be grouped as 1
        for (i, roof) in enumerate(roofs):
            if i % 500 == 0:
                print("Processed " + str(i) + "/" + str(len(roofs)) + " roofs")
            color = get_dominant_color_name(roof.img)
            try:
                roof.features['color'] = self.color_ids[color]
            except KeyError:
                self.ids += 1
                self.color_ids[color] = self.ids
                roof.features['color'] = self.ids


def closest_colour(requested_colour):
    min_colours = {}
    for key, name in webcolors.css3_hex_to_names.items():
        r_c, g_c, b_c = webcolors.hex_to_rgb(key)
        rd = (r_c - requested_colour[0]) ** 2
        gd = (g_c - requested_colour[1]) ** 2
        bd = (b_c - requested_colour[2]) ** 2
        min_colours[(rd + gd + bd)] = name
    return min_colours[min(min_colours.keys())]


def get_colour_name(requested_colour):
    try:
        closest_name = actual_name = webcolors.rgb_to_name(requested_colour, spec=u'css3')
    except ValueError:
        closest_name = closest_colour(requested_colour)
        actual_name = None
    return actual_name, closest_name


def get_dominant_color_name(image):
    image = image.resize((50, 50))  # optional - for faster processing
    ar = scipy.misc.fromimage(image)
    shape = ar.shape
    ar = ar.reshape(scipy.product(shape[:2]), shape[2])
    codes, dist = scipy.cluster.vq.kmeans(ar.astype(float), NUM_CLUSTERS)
    vecs, dist = scipy.cluster.vq.vq(ar, codes)  # assign codes
    counts, bins = scipy.histogram(vecs, len(codes))  # count occurrences
    index_max = scipy.argmax(counts)  # find most frequent
    peak = codes[index_max]
    colour = [int(l) for l in peak]
    actual_name, closest_name = get_colour_name(colour)
    # print("Actual colour name:", actual_name, ", closest colour name:", closest_name)
    return closest_name
