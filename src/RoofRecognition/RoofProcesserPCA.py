NUM_CLUSTERS = 5
from src.RoofRecognition.RoofProcesser import RoofProcesser
from sklearn.decomposition import RandomizedPCA
from PIL import Image
import numpy as np

class RoofProcesserPCA(RoofProcesser):
    def __init__(self):
        self.pca = None
        self.n_components = 17
        self.pca_feature_name = "pca"

    def process(self, roofs):
        print('Processing feature '+self.pca_feature_name)
        x_train = []
        x_train_classes = [[], [], [], []]
        for roof in roofs:
            image_array = self.get_image_array(roof)
            x_train_classes[roof.c - 1].append(image_array)
            x_train.append(image_array)
        if self.pca is None:
            self.pca = [[], [], [], []]
            for i in range(0, 4):
                self.pca[i] = RandomizedPCA(iterated_power=2, n_components=self.n_components, whiten=False)\
                            .fit(x_train_classes[i])
        x_in_principal_space = [[], [], [], []]
        for i in range(0, 4):
            x_in_principal_space[i] = self.pca[i].transform(x_train)
        for (i, roof) in enumerate(roofs):
            for j in range(0, 4):
                roof.features[self.pca_feature_name + str(j)] = x_in_principal_space[j][i]

    def get_image_array(self, roof):
        """
        Implement this function to make pca with different image representations
        :param roof: the roof to make an array of values for
        :return list of values representing the image
        """
        raise NotImplementedError("Select a specific PCA")

#TODO find the best params for resizing and number of components

class RoofProcesserGrayscalePCA(RoofProcesserPCA):
    def __init__(self):
        super().__init__()
        self.pca_feature_name = "grayscale_pca"
        self.n_components = 17

    def get_image_array(self, roof):
        image = roof.getResizedGrayScale()  # resize - pictures need to be with the same size
        data = image.getdata()
        return np.array(data)


class RoofProcesserRGBHistogramPCA(RoofProcesserPCA):
    def __init__(self):
        super().__init__()
        self.n_components = 20
        self.pca_feature_name = "rgb_hist_pca"

    def get_image_array(self, roof):
        image = roof.img
        image = image.resize((130, 130))  # resize - pictures need to be with the same size
        data = image.histogram()
        return data  # returns array with rgb histogram

class RoofProcesserHSVHistogramPCA(RoofProcesserPCA):
    def __init__(self):
        super().__init__()
        self.n_components = 21
        self.pca_feature_name = "hsv_hist_pca"

    def get_image_array(self, roof):
        image = roof.img
        image = image.resize((130, 130))  # resize - pictures need to be with the same size
        image = image.convert('HSV')
        return image.histogram() # returns array with rgb histogram

from skimage import data, io, filters
class RoofProcesserSobelPCA(RoofProcesserPCA):
    def __init__(self):
        super().__init__()
        self.n_components = 5
        self.pca_feature_name = "sobel_pca"

    def get_image_array(self, roof):
        image = roof.getGrayScale()
        image = image.resize((100, 100))  # resize - pictures need to be with the same size
        edges = filters.sobel(image)
        features = []
        for edge in edges:
            features+=list(edge)
        return np.array(features) # returns array with rgb histogram

from skimage.feature import daisy
class RoofProcesserDaisyPCA(RoofProcesserPCA):
    def __init__(self):
        super().__init__()
        self.pca_feature_name = "daisy_pca"
        self.n_components = 12

    def get_image_array(self, roof):
        image = roof.getResizedGrayScale()  # resize - pictures need to be with the same size
        descs= daisy(image, step=6, radius=20, rings=2, histograms=3,
                         orientations=10)
        data = [c[2] for c in descs]
        dataP = np.array(data)
        flattened = [item for sublist in dataP for item in sublist]
        return np.array(flattened)


from skimage import feature
from skimage.morphology import octagon
from skimage import img_as_float
class RoofProcesserPeaksPCA(RoofProcesserPCA):
    def __init__(self):
        super().__init__()
        self.pca_feature_name = "peaks_pca"
        self.n_components = 4

    def get_image_array(self, roof):
        image = roof.getGrayScale()
        img = img_as_float(image)

        allPeaks = feature.corner_peaks(img)
        roof.features['peaks_all_count'] = len(allPeaks)
        peaks = feature.corner_peaks(img,num_peaks = 50)
        cornerOrientations = feature.corner_orientations(img, peaks, octagon(3, 2))
        deg = np.rad2deg(cornerOrientations)

        return np.array(deg[:15])

class RoofProcesserPeaksPrositionPCA(RoofProcesserPCA):
    def __init__(self):
        super().__init__()
        self.pca_feature_name = "peaks_position_pca"
        self.n_components = 4

    def get_image_array(self, roof):
        image = roof.getGrayScale()
        img = img_as_float(image)

        allPeaks = feature.corner_peaks(img,num_peaks = 60)
        func = lambda x: x[0] * 100 + x[1]
        positions = list(map(func, allPeaks))

        return np.array(positions[:15])

from scipy import ndimage as ndi
class RoofProcesserLocalMaximumsPositionPCA(RoofProcesserPCA):
    def __init__(self):
        super().__init__()
        self.pca_feature_name = "local_maximums_positions_pca"
        self.n_components = 4

    def get_image_array(self, roof):
        image = roof.getGrayScale()
        image_max = ndi.maximum_filter(image, size=20, mode='constant')
        img = img_as_float(image_max)

        allPeaks = feature.corner_peaks(img, num_peaks = 60)
        func = lambda x: x[0] * 100 + x[1]
        positions = list(map(func, allPeaks))

        return np.array(positions[:15])

class RoofProcesserHOGPCA(RoofProcesserPCA):
    def __init__(self):
        super().__init__()
        self.pca_feature_name = "hog_pca"
        self.n_components = 16

    def get_image_array(self, roof):
        image = roof.getResizedGrayScale()
        img = img_as_float(image)
        hog = feature.hog(img)
        return np.array(hog)