__author__ = 'martin'
import numpy as np
class RoofEvaluator:

    def eval(self, roofs):
        correct = 0
        total = len(roofs)
        confusion = np.zeros((5,5))
        for roof in roofs:
            if roof.prediction == roof.c:
                correct+=1
            confusion[roof.c][roof.prediction] +=1
        return correct/float(total), [confusion[i][i]/np.sum(confusion[i]) for i in [1,2,3,4]],confusion