
from PIL import Image
from PIL import ImageStat
from DataSelectors import DataSelectors 
from SegmentsAnalysis import SegmentsAnalysis


def main():
    data = DataSelectors()
    img = data.getImage('69517115')
    sa = SegmentsAnalysis()
    r1 = sa.segments(img)
    img = data.getImage('923928600')
    r2 = sa.segments(img)


if __name__ == "__main__":
   main()
