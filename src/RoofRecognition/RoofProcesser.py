__author__ = 'martin'

import os.path
import csv
import SerializeOutput as serialize
import numpy

WRITE_TO_FILE = False

class RoofProcesser:

    """
    Abstract class. Extend this one and add it to the RoofPipeline to process images.
    """
    def process(self, roofs):
        raise NotImplementedError( "Define a Processer")


    def process_and_store(self, roofs):
        file_name = '../../features/'+self.__class__.__name__+'.csv'
        print(file_name)
        if WRITE_TO_FILE:
            self.process(roofs)
            self.write_features_to_file(roofs, file_name)
        else:            
            if os.path.exists(file_name):
                print('reading images from file...', file_name)
                features = self.read_features_from_file(file_name)
                for roof in roofs:
                    #print(features[roof.id])
                    #roof.features = features[roof.id]
                    for key,value in features[roof.id].items():
                        roof.features[key] = value
            else:
                self.process(roofs)
                self.write_features_to_file(roofs, file_name)


    def read_features_from_file(self, file_name):
        features = {}
        print('reading from file...')
        with open(file_name, newline='') as csvfile:
            csvreader = csv.reader(csvfile, delimiter=',', quotechar='|')
            headers = []
            for i,row in enumerate(csvreader):
                features_for_image = {}
                image_id = 0
                if i == 0:
                    headers = row
                else:
                    for j,value in enumerate(row):
                        if j == 0:
                            image_id = row[j]
                        else:
                            feat_value = row[j]
                            feat = None
                            if str(feat_value).startswith('['):
                                feat = []
                                feat_arr = feat_value.replace('[', '').replace(']', '').split(' ')
                                for f in feat_arr:
                                    if (len(f.strip()) > 0):
                                        feat.append(float(f))
                                features_for_image[headers[j]] = numpy.array(feat)
                            else:
                                feat = float(feat_value)
                                features_for_image[headers[j]] = feat
                        if j == len(row)-1:
                            #print('features:', image_id, features_for_image)
                            features[image_id] = features_for_image
        return features

    def write_features_to_file(self, roofs, file_name):        
        if not os.path.exists(file_name):
            headers = ['Id']
            headers.extend(list(roofs[0].features.keys()))
            serialize.write_to_csv_file(headers, file_name)
        for roof in roofs:
            feat = [roof.id]
            feat.extend(list(roof.features.values()))
            serialize.write_to_csv_file(feat, file_name)