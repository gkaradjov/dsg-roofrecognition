from PIL import Image
from src.RoofRecognition.DataSelectors import DataSelectors
import random
import numpy as np
from scipy.ndimage import convolve
from skimage.util import random_noise, img_as_float
from skimage import io, exposure, img_as_uint, img_as_float

class TrainDataBooster:
    def __init__(self, train_dataset = 'id_train.csv'):
        self.rootRawImagesPath = '../../images/Raw/'
        self.max_id = 923928600
        self.id_train_boosted = dict()
        self.data_selectors = DataSelectors(train_dataset=train_dataset)

    def rotate(self, image, angle):
        return image.rotate(angle)

    def mirror_horizontal(self, image):
        return image.transpose(Image.FLIP_TOP_BOTTOM)

    def mirror_vertical(self, image):
        return image.transpose(Image.FLIP_LEFT_RIGHT)

    def boost_rotating(self, from_class, to_class, rotate_angles):
        images = self.data_selectors.get_image_ids_of_class(from_class)
        for key in images.keys():
            for angle in rotate_angles:
                image = self.data_selectors.getImage(key)
                new_image = self.rotate(image,angle)
                self.save_new_image(to_class, new_image)

    def boost_mirroring(self, class_id):
        images = self.data_selectors.get_image_ids_of_class(class_id)
        for key in images.keys():
            image = self.data_selectors.getImage(key)
            new_image = self.mirror_horizontal(image)
            self.save_new_image(class_id, new_image)

            image = self.data_selectors.getImage(key)
            new_image = self.mirror_vertical(image)
            self.save_new_image(class_id, new_image)


    def boost_class2(self, number_to_boost):
        images_1st_class = list(self.data_selectors.get_image_ids_of_class(1).keys())
        images_2nd_class = list(self.data_selectors.get_image_ids_of_class(2).keys())
        for i in range(0,number_to_boost):
            action = random.randint(1, 4)
            if action == 1: #rotate on 90 degrees an image from 1st class
                i = random.randint(0, len(images_1st_class)-1)
                image_src = self.data_selectors.getImage(images_1st_class[i])
                new_image = self.rotate(image_src,90)
                self.save_new_image(2, new_image)
                del images_1st_class[i]
            elif action == 2: # rotate on 270 degrees an image from 2nd class
                i = random.randint(0, len(images_1st_class)-1)
                image_src = self.data_selectors.getImage(images_1st_class[i])
                new_image = self.rotate(image_src,270)
                self.save_new_image(2, new_image)
                del images_1st_class[i]
            elif action == 3: # flip horizontally
                i = random.randint(0, len(images_2nd_class)-1)
                image_src = self.data_selectors.getImage(images_2nd_class[i])
                new_image = self.mirror_horizontal(image_src)
                self.save_new_image(2, new_image)
                del images_2nd_class[i]
            elif action == 4: # flip vertically
                i = random.randint(0, len(images_2nd_class)-1)
                image_src = self.data_selectors.getImage(images_2nd_class[i])
                new_image = self.mirror_vertical(image_src)
                self.save_new_image(2, new_image)
                del images_2nd_class[i]

    def boost_class1(self, number_to_boost):
        images_1st_class = list(self.data_selectors.get_image_ids_of_class(1).keys())
        images_2nd_class = list(self.data_selectors.get_image_ids_of_class(2).keys())
        for i in range(0,number_to_boost):
            action = random.randint(1, 4)
            if action == 1: #rotate on 90 degrees an image from 1st class
                i = random.randint(0, len(images_2nd_class)-1)
                image_src = self.data_selectors.getImage(images_2nd_class[i])
                new_image = self.rotate(image_src,90)
                self.save_new_image(2, new_image)
                del images_1st_class[i]
            elif action == 2: # rotate on 270 degrees an image from 2nd class
                i = random.randint(0, len(images_2nd_class)-1)
                image_src = self.data_selectors.getImage(images_2nd_class[i])
                new_image = self.rotate(image_src,270)
                self.save_new_image(2, new_image)
                del images_1st_class[i]
            elif action == 3: # flip horizontally
                i = random.randint(0, len(images_1st_class)-1)
                image_src = self.data_selectors.getImage(images_1st_class[i])
                new_image = self.mirror_horizontal(image_src)
                self.save_new_image(2, new_image)
                del images_2nd_class[i]
            elif action == 4: # flip vertically
                i = random.randint(0, len(images_1st_class)-1)
                image_src = self.data_selectors.getImage(images_1st_class[i])
                new_image = self.mirror_vertical(image_src)
                self.save_new_image(2, new_image)
                del images_2nd_class[i]

    def boost_same_class(self, number_to_boost, class_id):
        images_from_class = list(self.data_selectors.get_image_ids_of_class(class_id).keys())
        used_image_ids = dict()
        for i in range(0,number_to_boost):
            action = random.randint(1, 5)
            if action == 1: #rotate on 90 degrees
                i = random.randint(0, len(images_from_class)-1)
                image_src = self.data_selectors.getImage(images_from_class[i])
                new_image = self.rotate(image_src,90)
                self.save_new_image(class_id, new_image)
            elif action == 2: # rotate on 180 degrees
                i = random.randint(0, len(images_from_class)-1)
                image_src = self.data_selectors.getImage(images_from_class[i])
                new_image = self.rotate(image_src,180)
                self.save_new_image(class_id, new_image)
            elif action == 3: # rotate on 270 degrees
                i = random.randint(0, len(images_from_class)-1)
                image_src = self.data_selectors.getImage(images_from_class[i])
                new_image = self.rotate(image_src,270)
                self.save_new_image(class_id, new_image)
            elif action == 4: # flip horizontally
                i = random.randint(0, len(images_from_class)-1)
                image_src = self.data_selectors.getImage(images_from_class[i])
                new_image = self.mirror_horizontal(image_src)
                self.save_new_image(class_id, new_image)
            elif action == 5: # flip vertically
                i = random.randint(0, len(images_from_class)-1)
                image_src = self.data_selectors.getImage(images_from_class[i])
                new_image = self.mirror_vertical(image_src)
                self.save_new_image(class_id, new_image)

    def boost_class3(self, number_to_boost):
        self.boost_same_class(number_to_boost, 3)

    def boost_class4(self, number_to_boost):
        self.boost_same_class(number_to_boost, 4)

    def write_id_train_boosted(self):
        self.id_train_boosted.update(self.data_selectors.imageDictionary)
        with open('../../images/id_train_boosted.csv', 'w') as f:
            [f.write('{0},{1}\n'.format(key, value)) for key, value in self.id_train_boosted.items()]

    def save_new_image(self, class_id, image, is_array = False):
        self.max_id+=1
        image_name = self.rootRawImagesPath+str(self.max_id)+".jpg"
        if is_array:
            io.imsave(image_name, image)
        else:
            image.save(image_name)
        self.id_train_boosted[str(self.max_id)] = class_id

    def boost_additional_class(self, class_to_boost):
        images_from_class = list(self.data_selectors.get_image_ids_of_class(class_to_boost).keys())
        upper_range = len(images_from_class)
        for i in range(0,int(upper_range)):
            image_src = self.data_selectors.getImage(images_from_class[i])
            float_image = img_as_float(image_src)
            noisy_image = random_noise(float_image)
            im = img_as_uint(noisy_image)
            self.save_new_image(class_to_boost, im, is_array=True)

        for i in range(0,int(upper_range)):
            image_src = self.data_selectors.getImage(images_from_class[i])
            float_image = img_as_float(image_src)
            noisy_image = random_noise(float_image, mode='poisson')
            im = img_as_uint(noisy_image)
            self.save_new_image(class_to_boost, im, is_array=True)

        for i in range(0,int(upper_range)):
            image_src = self.data_selectors.getImage(images_from_class[i])
            float_image = img_as_float(image_src)
            noisy_image = random_noise(float_image, mode='s&p')
            im = img_as_uint(noisy_image)
            self.save_new_image(class_to_boost, im, is_array=True)

        for i in range(0,int(upper_range)):
            image_src = self.data_selectors.getImage(images_from_class[i])
            angle_upper_bound = 359
            if class_to_boost < 3:
                angle_upper_bound = 30
            angle = random.randint(1, angle_upper_bound)
            new_image = self.rotate(image_src,angle)
            self.save_new_image(class_to_boost, new_image)

    def boost_main(self):

        images_class1 = 3479
        images_class2 = 1856
        images_class3 = 859
        images_class4 = 1806

        print("Boosting class 2")
        self.boost_class2(images_class1-images_class2)
        print("Boosting class 3")
        self.boost_class3(images_class1-images_class3)
        print("Boosting class 4")
        self.boost_class4(images_class1-images_class4)

        print("Writing id_train_boosted.csv file to images folder")
        self.write_id_train_boosted()

    def boost_additional(self):
        self.boost_additional_class(1)
        self.boost_additional_class(2)
        self.boost_additional_class(3)
        self.boost_additional_class(4)
        self.write_id_train_boosted()


#this is run to make classes with equal number of instances
#should be run with data selectors id_train!
t = TrainDataBooster()
t.boost_main()
max_id = t.max_id
print(t.max_id)

#this is run to make classes with double number of instances
#should be run with dataselectors id_train_boosted!
t = TrainDataBooster('id_train_boosted.csv')
t.max_id = max_id
# t.max_id = 923934516
t.boost_additional()

