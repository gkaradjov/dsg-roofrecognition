from sklearn.preprocessing import Normalizer, RobustScaler, StandardScaler
import numpy

# Needs to be tested on test(kaggle) - different accuracy with different params
class RoofNormalizer:
    def normalize_feature(self, feature_name, roofs_train, roofs_test):
        print("Normalizing feature "+feature_name)

        feature_list_train = [roof.features[feature_name] for roof in roofs_train]
        feature_list_test = [roof.features[feature_name] for roof in roofs_test]

        normalizer = Normalizer()
        normalizer.fit(feature_list_train+feature_list_test)

        feature_normalized_train = normalizer.transform(feature_list_train)
        feature_normalized_test = normalizer.transform(feature_list_test)

        # uncomment with Normalizer(), comment with StandardScaler and RobustScaler
        if not type(feature_list_test[0]) == numpy.ndarray:
            feature_normalized_test = feature_normalized_test[0]
            feature_normalized_train = feature_normalized_train[0]

        for (i,roof) in enumerate(roofs_train):
            roof.features[feature_name] = feature_normalized_train[i]
        for (i,roof) in enumerate(roofs_test):
            roof.features[feature_name] = feature_normalized_test[i]