__author__ = 'martin'

import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)

from src.RoofRecognition.HoughTransform import HoughTransform
from src.RoofRecognition.Histogram import Histogram
from src.RoofRecognition.DataSelectors import DataSelectors
from src.RoofRecognition.Roof import Roof
from src.RoofRecognition.RoofEvaluator import RoofEvaluator
from src.RoofRecognition.RoofProcesserSize import RoofProcesserSize
from src.RoofRecognition.SegmentsAnalysis import SegmentsAnalysis
from src.RoofRecognition.RoofProcesserPCA import RoofProcesserGrayscalePCA, RoofProcesserRGBHistogramPCA, RoofProcesserHSVHistogramPCA, RoofProcesserSobelPCA, RoofProcesserDaisyPCA,RoofProcesserPeaksPCA,RoofProcesserPeaksPrositionPCA, RoofProcesserLocalMaximumsPositionPCA, RoofProcesserHOGPCA
from src.RoofRecognition.RoofProcesserColor import RoofProcesserColor, RoofProcesserFrequentColors
import src.RoofRecognition.SerializeOutput as SerializeOutput
from sklearn.preprocessing import Normalizer
from sklearn.svm import *
from sklearn.naive_bayes import *
from sklearn.grid_search import *
from sklearn.preprocessing import *
from sklearn.pipeline import *
from sklearn.feature_selection import *
import numpy
dev = True
crossvalidation = False
run_version = '1'

def normalize(feature_name, roofs_normalize):
    feature_list = [roof.features[feature_name] for roof in roofs_normalize]
    normalizer = Normalizer()
    feature_normalized = normalizer.fit_transform(feature_list)
    if not type(feature_list[0])==numpy.ndarray:
        feature_normalized = feature_normalized[0]
    for (i,roof) in enumerate(roofs_normalize):
        roof.features[feature_name] = feature_normalized[i]

def getFeatures(roof_dataset):
    X = []
    for roof in roof_dataset:
        roof_x=[]
        for feature in feature_list:
            roof.features[feature]
            if type(roof.features[feature])==numpy.ndarray:
                roof_x+=roof.features[feature].tolist()
            else:
                roof_x.append(roof.features[feature])
        X.append(roof_x)
    return X

selector = DataSelectors()
classes = [1,2,3,4]


def getRoofs(fold, dev):
    roofs = []
    evaluationRoofs = []
    if dev:
        for c in classes:
        ##We hold out 20% for evaluation in dev
            imageIds = list(selector.get_image_ids_of_class(c).keys())
            sliceLength = round(len(imageIds)/5)
            train = imageIds[0:fold*sliceLength] + imageIds[(fold+1)*sliceLength:]
            test = imageIds[fold*sliceLength:(fold+1)*sliceLength]
            for imageId in train:
                roofs.append(Roof(imageId,selector.getImage(imageId),c))
            for imageId in test:
                evaluationRoofs.append(Roof(imageId,selector.getImage(imageId),c))
    else:
        # We are using the whole train set for training
        for c in classes:
            imageIds = selector.get_image_ids_of_class(c)
            for imageId in imageIds:
                roofs.append(Roof(imageId,selector.getImage(imageId),c))
        # Read the eval ids as an evaluation set
        imageIds = selector.get_image_ids_eval()
        for imageId in imageIds:
            evaluationRoofs.append(Roof(imageId,selector.getImage(imageId),4))
    return roofs, evaluationRoofs
##train

pipeline =[]
#pipeline.append(HoughTransform())
#pipeline.append(Histogram())
#pipeline.append(RoofProcesserSize())
#pipeline.append(SegmentsAnalysis())
#pipeline.append(CornersAnalysis())
#pipeline.append(RoofProcesserGrayscalePCA())
#pipeline.append(RoofProcesserRGBHistogramPCA())
#pipeline.append(RoofProcesserHSVHistogramPCA())
#pipeline.append(RoofProcesserSobelPCA())
#pipeline.append(RoofProcesserDaisyPCA())
#pipeline.append(RoofProcesserPeaksPCA())
#pipeline.append(RoofProcesserPeaksPrositionPCA())
#pipeline.append(RoofProcesserLocalMaximumsPositionPCA())
#pipeline.append(RoofProcesserFrequentColors())
pipeline.append(RoofProcesserHOGPCA())



# CrossValidation parameter
folds = 5
if not dev or not crossvalidation:
    folds = 1

evaluations = []
evaluator = RoofEvaluator()



for fold in range(folds):

    roofs, evaluationRoofs = getRoofs(fold, dev)

    for processer in pipeline:
        processer.process_and_store(roofs)
        processer.process_and_store(evaluationRoofs)

    ##normalize
    feature_list = list(roofs[0].features.keys())
    # for feature in feature_list:
    #     normalize(feature, roofs)
    #     normalize(feature, evaluationRoofs)

    print('features found: ', str(len(feature_list)))


    ##Someone do something - svm/neuralnet/learning over roof.features
    ##And then we will iterate over the pipeline for eval
    X = getFeatures(roofs)
    y = [roof.c for roof in roofs]
    X_eval = getFeatures(evaluationRoofs)


    # Scale features from 0 to 1
    min_max_scaler = MinMaxScaler()
    X = min_max_scaler.fit_transform(X)
    X_eval = min_max_scaler.transform(X_eval)

    ## TODO needs another/better tuned classifier
    #clf = LinearSVC(C=4)

    # Apply feature selection
    clf = Pipeline([
        ('feature_selection', SelectPercentile(f_classif, percentile=70)),
        ('classification', LinearSVC(C=4))
    ])

    # If you want to run Grid search, uncomment the following lines and comment the classifier above.
    # The best results were with linear kernel and C=the max available value (16, 32, 100 etc.)
    # param_grid = [
    #   {'C': [1, 2, 4, 8, 16, 32], 'kernel': ['linear']},
    #   {'C': [1, 2, 4, 8, 16, 32], 'gamma': [0.01, 0.001, 0.0001], 'kernel': ['rbf']},
    #  ]
    # svr = SVC()
    # clf = GridSearchCV(svr, param_grid)

    

    clf.fit(X, y)

    ##And classify based on the extracted features.
    y_true = clf.predict(X_eval)
    for (i,roof) in enumerate(evaluationRoofs):
        roof.prediction = y_true[i]

    ## Serialize the result
    SerializeOutput.serialize(evaluationRoofs, run_version)

    ## And evaluate

    evaluations.append(evaluator.eval(evaluationRoofs))
    print(evaluations[-1])
    if isinstance(clf, GridSearchCV):
        print("best params:")
        print(clf.best_params_)
        print("best score:")
        print(clf.best_score_)
        print("best estimator:")
        print(clf.best_estimator_)

if dev:
    meanScore = numpy.mean([x[0] for x in evaluations])
    stdScore = numpy.std([x[0] for x in evaluations])
    maxScore = max([x[0] for x in evaluations])
    minScore = min([x[0] for x in evaluations])
    print("Mean score", meanScore, "Std Score", stdScore, "Max Score", maxScore, "Min Score", minScore)
    for ind,evaluation in enumerate(evaluations):
        print ("Experiment",ind)
        print("Accuracy",evaluation[0])
        print("Accuracy per class",evaluation[1])
        print("Confusion Matrix",evaluation[2])

