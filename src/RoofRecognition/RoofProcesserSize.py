from src.RoofRecognition.RoofProcesser import RoofProcesser
import os


class RoofProcesserSize(RoofProcesser):
     def process(self, roofs):
         for (i,roof) in enumerate(roofs):
              width, height = roof.img.size
              # roof.features['width'] = width
              # roof.features['height'] = height
              roof.features['dif'] = width-height
              size = os.stat('../../images/Raw/'+str(roof.id)+'.jpg').st_size
              roof.features['size'] = size