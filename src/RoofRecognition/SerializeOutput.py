import csv
import os

def serialize(roofs, version):
    ensure_directory_exists('../../output')
    file = '../../output/out-'+version+'.csv'
    write_to_csv_file(['Id','label'], file)
    for roof in roofs:
        result = []
        result.append(roof.id)
        result.append(roof.prediction)
        write_to_csv_file(result, file)


def write_to_csv_file(array, file_path):
    with open(file_path, 'a+', newline='') as csvfile:
        csv_writer = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        csv_writer.writerow(array)

def ensure_directory_exists(dir_path):
   # If the given directory does not exist - create it
   if not os.path.exists(dir_path):
      print("Creating directory:" + dir_path)
      os.makedirs(dir_path)