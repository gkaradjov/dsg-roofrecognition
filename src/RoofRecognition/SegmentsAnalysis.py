from PIL import ImageDraw
from RoofProcesser import RoofProcesser
from skimage import img_as_float
import numpy as np
from scipy import ndimage
import skimage
from skimage import filters
from skimage import measure
from functools import reduce
from skimage.segmentation import mark_boundaries
import matplotlib.pyplot as plt
from skimage import exposure
from skimage.morphology import disk
from skimage.filters import rank
from sklearn.feature_extraction import image
from sklearn.cluster import spectral_clustering



class SegmentsAnalysis(RoofProcesser):
    def process(self, roofs):
        for (i,roof) in enumerate(roofs):
            if (i % 200 == 0):
                print("Processed " + str(i) + "/" + str(len(roofs)) + " roofs")
            self.segments(roof)
 
    def segments(self, roof):
        gray_img = roof.getGrayScale()
        img = img_as_float(gray_img)

        segments = skimage.segmentation.felzenszwalb(img, scale=400, sigma=0.8, min_size=150)
        
        find = ndimage.measurements.find_objects(segments)
        length = len(find)
        sizes = np.array([[s.stop - s.start for s in object_slice] 
              for object_slice in find])
        areas = np.array([sz[0] * sz[1] for sz in sizes])

        roof.features['segments_count'] = len(list(filter(lambda x: x > 4, areas)))
        f = lambda x: x[0] > x[1]
        roof.features['segments_is_portrait_oriented'] = max(list(map(f, sizes)),default=0)
#        roof.features['segments_max_proportion'] = max(areas, default = 0) / roof.features['size']

        #spectrum = np.fft.fftshift(np.fft.fft2(img))
        #intensity = 10*np.log(np.abs(spectrum))


        #mask = intensity.astype(bool)
        #img = intensity.astype(float)
        #graph = image.img_to_graph(img, mask=mask)
        #graph.data = np.exp(-graph.data/graph.data.std())

        #labels = spectral_clustering(graph)
        #label_img = -np.ones(mask.shape)
        #label_img[mask] = labels
        

        #roof.features['segments_long_count'] = len(list(filter(lambda x: x[0] > x[1], sizes)))
        #roof.features['segments_width_count'] = len(list(filter(lambda x: x[0] < x[1], sizes)))



