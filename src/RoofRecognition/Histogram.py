from PIL import ImageDraw
from RoofProcesser import RoofProcesser
from skimage import img_as_float
import numpy as np
from enum import Enum

class Histogram(RoofProcesser):
    def process(self, roofs):
        for (i,roof) in enumerate(roofs):
            if (i%500 ==0):
                print ("Processed "+str(i)+"/"+str(len(roofs))+" roofs")
            self.histogram(roof)
 
    def histogram(self, roof):
        fImage = img_as_float(roof.img)
        bin_counts, t = np.histogram(fImage, 7)
        roof.features['histogram'] = bin_counts
        roof.features['histogram_form'], roof.features['histogram_form_possition'] = self.calculateForm(bin_counts)
        a=1

    def calculateForm(self, bin_counts):
        mean = np.mean(bin_counts)
        variance  = np.std(bin_counts)
        length = len(bin_counts)

        if variance < (mean / 2):
            return HistogramForm.EquallyDistributed.value, (len(bin_counts)/2)

        if (bin_counts[0] + bin_counts[1]) / 2 > (mean + variance) and  (bin_counts[length -1 ] + bin_counts[length - 2]) / 2 > (mean + variance):
            return HistogramForm.ReversedBell.value, -1
        for i,x in enumerate(bin_counts):
            if i == 0 or i == (length - 1):
                continue
            if x > (mean + variance) and bin_counts[i-1] < mean and bin_counts[i+1] < mean:
                return HistogramForm.NarrowBell.value, i
            if x > (mean + variance) and bin_counts[i-1] > mean and bin_counts[i+1] > mean:
                return HistogramForm.NormalBell.value, i

        return HistogramForm.Other.value, -1



class HistogramForm(Enum):
    Other = 0
    NormalBell = 1
    NarrowBell = 2
    EquallyDistributed = 3
    ReversedBell = 4


